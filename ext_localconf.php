<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelscptcntfacts',
	array(
		'Fact' => 'list',
		
	),
	// non-cacheable actions
	array(
		'Fact' => '',
		
	)
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder